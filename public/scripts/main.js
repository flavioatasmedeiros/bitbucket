
var defaultParams = {pagelen: 100};
var items = [];


var Model = function(data) {  
  _.extend(this, data);
};


var Repo = function() {
  Model.apply(this, arguments);
  var s = this.name.replace(/[\|-]/g, '').split('  ');
  return _.extend(this, {
    code: s[0], 
    agency: s[1], 
    client: s[2], 
    project: s[3]
  });
};


Repo.prototype = Object.create(Model.prototype);  


function digitalboxcc() {
  return repositories(items, 'digitalboxcc');
}


function repositories(items, username) {
  return fetch(items, 'repositories/' + username, Repo);
}


function fetch(col, path, Obj) {
  return service(path).then(createCol(addToCol(col, Obj)));
}


function createCol(adder) {
  return function(response) {
    _.each(response.values, adder);
  };
}


function addToCol(col, Obj) {
  return function(val) {
    col.push(new Obj(val));
  };
}
  

function service(path, params) {  
  return $.getJSON(
    '/service/' + path, 
    _.extend({}, defaultParams, params || {})
  );
}



digitalboxcc();