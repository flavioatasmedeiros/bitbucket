<?php

App::missing(function() 
{
  return '<center><h1>404</h1></center>';
});


Route::get('/', function() 
{
  return View::make('main');
});


Route::get('/service/{slug}', function($slug)
{
  $input = Input::all();

  return service($slug, $input);

})->where('slug', '([A-z\d-\/_.]+)?');


function service($path, $query = array()) 
{
  $rootUrl = 'https://api.bitbucket.org/2.0/';
  $authKey = 'ZmxhdmlvYXRhc21lZGVpcm9zOnhhcm9wZQ==';
  
  $response = Requests::get(
    $rootUrl.$path.'?'.http_build_query($query), 
    array('Authorization' => "Basic $authKey")
  );

  return str_replace($rootUrl, '', $response->body);
}
